import React from 'react';
import { Link } from 'react-router-dom';
import cx from 'classnames';

import logo from 'images/logo.png';
import './style.css';

export default class App extends React.Component {
  render() {
    return (
      <div className={cx('App')}>
        <Header />
      </div>
    );
  }
}

function Header() {
  return <div className="header">
    <div className="contact">
      <div className="callback">
        <Link to='/contacts'>Оставить заявку</Link>
      </div>
      <div className="logo">
        <Link to='/'>
          <img src={logo} alt="" />
        </Link>
      </div>
      <div className="phone">
        <div className="number">8 (900) 018-93-33</div>
        <div className="annotation">(Запись на фотосессию)</div>
      </div>
    </div>
    <Menu />
  </div>
}

function Menu() {
  return <div className="menu">
    Menu
  </div>;
}